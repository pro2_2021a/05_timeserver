package guiModel;

import businessModel.ServerLog;
import businessModel.ServerLogItem;

import javax.swing.*;

public class ServerLogListModel extends AbstractListModel
{
    public ServerLogListModel(ServerLog serverLog)
    {
        this.serverLog = serverLog;
    }

    ServerLog serverLog;

    @Override
    public int getSize()
    {
        return serverLog.GetCount();
    }

    @Override
    public Object getElementAt(int index)
    {
        return serverLog.GetItem(index);
    }

    public void addItem(ServerLogItem serverLogItem)
    {
        serverLog.addItem(serverLogItem);
        fireIntervalAdded(
                this,
                serverLog.GetCount()-1,
                serverLog.GetCount()-1);
    }
}
