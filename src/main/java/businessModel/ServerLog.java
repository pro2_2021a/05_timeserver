package businessModel;

import java.util.ArrayList;

public class ServerLog
{
    ArrayList<ServerLogItem> serverLogItems = new ArrayList<>();

    public void addItem(ServerLogItem serverLogItem)
    {
        serverLogItems.add(serverLogItem);
    }

    public ServerLogItem GetItem(int index)
    {
        return serverLogItems.get(index);
    }

    public int GetCount()
    {
        return serverLogItems.size();
    }
}
