package businessModel;

public class ServerLogItem
{
    public ServerLogItem(String client) {
        this.client = client;
    }

    String client;

    @Override
    public String toString() {
        return "Client: " + client;
    }
}
