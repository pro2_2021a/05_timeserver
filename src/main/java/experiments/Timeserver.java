package experiments;

import businessModel.ServerLogItem;
import guiModel.ServerLogListModel;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.time.LocalDateTime;

public class Timeserver
{
    public static void Listen(ServerLogListModel serverLogListModel) throws IOException
    {
        ServerSocket serverSocket = new ServerSocket(170);

        while (true)
        {
            Socket socketConnection = serverSocket.accept();

            PrintWriter printWriter = new PrintWriter(
                    socketConnection.getOutputStream());

            printWriter.println(LocalDateTime.now());
            printWriter.close();
            socketConnection.close();
            serverLogListModel.addItem(
                    new ServerLogItem(
                            socketConnection.getInetAddress().toString()));
        }

    }
}
