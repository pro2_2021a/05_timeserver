package experiments;

import businessModel.ServerLog;
import businessModel.ServerLogItem;
import gui.MainFrame;
import guiModel.ServerLogListModel;

import javax.swing.*;
import java.io.IOException;

public class TestClass
{
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run()
                    {
                        // testovaci data
                        ServerLog serverLog = new ServerLog();
                        serverLog.addItem(new ServerLogItem("Test1"));
                        serverLog.addItem(new ServerLogItem("Test2"));
                        //
                        ServerLogListModel serverLogListModel =
                                new ServerLogListModel(serverLog);

                        new MainFrame(serverLogListModel);

                        Thread listening = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Timeserver.Listen(serverLogListModel);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        listening.start();


                    }
                }
        );


    }
}
