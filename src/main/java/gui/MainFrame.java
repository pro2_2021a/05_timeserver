package gui;

import businessModel.ServerLog;
import businessModel.ServerLogItem;
import guiModel.ServerLogListModel;

import javax.swing.*;

public class MainFrame extends JFrame
{
    JList list;

    public MainFrame(ServerLogListModel serverLogListModel){
        setTitle("Timeserver");
        setSize(800,600);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        list = new JList();
        list.setModel(serverLogListModel);
        add(list);
    }
}
